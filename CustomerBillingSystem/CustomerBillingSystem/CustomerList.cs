﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerBillingSystem
{
    /**
    * Customerlist class that holds a list of customers
    * This class also uses events and delegates to notify its delegates when the list has changed
    * Author: Prince Nimoh
    * Context: RAD programming lab 2
    * Reference: ProductList class from textbook example for ProductMaintainance Project
    * Student #: 000792122
    * Date: June 6, 2018
    * */
    class CustomerList
    {
        //A list of customers
        private List<Customer> customers;

        //Delegate property for the customer list
        public delegate void ChangeHandler(CustomerList products);

        //Event for the customer list
        public event ChangeHandler Changed;

        
        //Property for total charge for residential customers
        public decimal ResidentialTotal
        {
            get
            {
                decimal residentialTotal = 0; //varial for total charge for residential customers
                for (int i = 0; i < Count(); i++)
                {
                    if (customers[i].CustomerType == "R")
                    {
                        residentialTotal+= customers[i].CustomerCharge;
                    }
                }
                return residentialTotal;
            }
        }

        
        //Property for total charge for commercial customers
        public decimal CommercialTotal
        {
            get
            {
                decimal commercialTotal = 0; //variable for total charge for commercial customers
                for (int i = 0; i < Count(); i++)
                {
                    if (customers[i].CustomerType == "C")
                    {
                        commercialTotal += customers[i].CustomerCharge;
                    }
                }
                return commercialTotal;

            }
        }


       
        //Property for total charge for industrial customers
        public decimal IndustrialTotal
        {
            get
            {
                 decimal industrialTotal = 0; //variable for total charge for industrial customers
                for (int i = 0; i < Count(); i++)
                {
                    if (customers[i].CustomerType == "I")
                    {
                        industrialTotal += customers[i].CustomerCharge;
                    }
                }
                return industrialTotal;
            }
        } 

        //Calculates and returns the total charge for all customers
        public decimal GrandTotal
        {
            get
            {
                return ResidentialTotal + CommercialTotal + IndustrialTotal;
            }
        }
       

        //Constructor for the customer list class
        public CustomerList()
        {
            customers = new List<Customer>();
        }

        //Method returns the number of customers in the CustomerList class
        public int Count() => customers.Count;

        //An indexer for the CustomerList Class
        //Throws an ArgumentOutOfRangeException if the parameter is out of bounds
        public Customer this[int i]
        {
            get
            {
                if (i < 0)
                {
                    throw new ArgumentOutOfRangeException("i");
                }
                else if (i >= customers.Count)
                {
                    throw new ArgumentOutOfRangeException("i");
                }
                return customers[i];
            }
            set
            {
                customers[i] = value;
                Changed(this);
            }
        }

        //Updates a CustomerList with information stored on file
        //Using the CustomerDB class
        public void Fill() => customers = CustomerDB.GetCustomers();

        //Saves current customer list to file
        public void Save() => CustomerDB.SaveCustomers(customers);

        //Adds a customer to the customerlist
        public void Add(Customer newcustomer)
        {
            customers.Add(newcustomer);
            Changed(this); //notify you deleagate of change
        }

        //Removes a customer from the customerlist
        public void Remove(Customer c)
        {
            customers.Remove(c);
            Changed(this);
        }
    }
}

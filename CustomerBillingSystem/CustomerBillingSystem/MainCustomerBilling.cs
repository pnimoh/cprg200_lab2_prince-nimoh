﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 * Author: Prince Nimoh
 * Student #: 000792122
 * Date: June 13, 2018
 * The application is a customer billing system.
 * It calculates the amount due for residential, commercial, and industrial
 * customers based on their electricity usage.
 * */
namespace CustomerBillingSystem
{
    public partial class MainCustomerBilling : Form
    {
     
        private BillingCases activeBillingCase; //Variable to track type of calculation case
        private CustomerList customers = new CustomerList();//list of customers
      

        /*
         * An enum type for the different billing cases
         * */
        enum BillingCases
        {
            Residential, //Billing case for residential customers
            Commercial, //Billing case for commercial customers
            Industrial  //Billing case for industrial customers
        }

        public MainCustomerBilling()
        {
            InitializeComponent();
        }

        /*
         * Method sets up the form to receive input for residential customers
         * when the form loads.
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            
            SetFormDefaults();   //Setup the residential defaults for the form
            FocusOnActiveCase(); //Focus the cursor on the text box of the active case
            customers.Changed += new CustomerList.ChangeHandler(HandleChange);
            customers.Fill();
            FillListBox();
            UpdateTotals();
        }

        /*
         * Method hides the UI control for entering usage for residential and commercial customers
         * */
        private void HideUIForGeneralUsage()
        {
            lblGeneralUsage.Visible = false;
            txtGeneralUsage.Visible = false;
        }

        /*
         * Method displays the UI control for entering usage for residential and commercial customers
         * */
        private void ShowUIForGeneralUsage()
        {
            lblGeneralUsage.Visible = true;
            txtGeneralUsage.Visible = true;
        }

        /*Method hides the UI controls for the entering usage for the industrial customers
         *
         **/
        private void HideUIForPeakUsage()
        {
            lblOffPeakUsage.Visible = false;
            txtOffPeakUsage.Visible = false;
            lblPeakUsage.Visible = false;
            txtPeakUsage.Visible = false;
        }

        /*Method shows the UI controls for entering usage for the industrial customers
         *
         **/
        private void ShowUIForPeakUsage()
        {
            lblOffPeakUsage.Visible = true;
            txtOffPeakUsage.Visible = true;
            lblPeakUsage.Visible = true;
            txtPeakUsage.Visible = true;
        }

        /*Method sets up the UI controls for entering usage for residential customers
         * 
         * */
        private void radBtnResidential_CheckedChanged(object sender, EventArgs e)
        {
            ShowUIForGeneralUsage();
            HideUIForPeakUsage();
            activeBillingCase = BillingCases.Residential;
            clearInputsAndOutputs();
            FocusOnActiveCase(); 
        }

        /*Method sets up the UI controls for entering usage for commercial customers
         * */
        private void radBtnCommercial_CheckedChanged(object sender, EventArgs e)
        {
            ShowUIForGeneralUsage();
            HideUIForPeakUsage();
            activeBillingCase = BillingCases.Commercial;
            clearInputsAndOutputs();
            FocusOnActiveCase();
        }

        /*Method sets up the UI controls for entering usage for industrial customers
         * */
        private void radBtnIndustrial_CheckedChanged(object sender, EventArgs e)
        {
            ShowUIForPeakUsage();
            HideUIForGeneralUsage();
            activeBillingCase = BillingCases.Industrial;
            clearInputsAndOutputs();
            FocusOnActiveCase();
        }

        /*
         * Method called when the calculatebill button is clicked
         * */
        private void btnCalculateBill_Click(object sender, EventArgs e)
        {

            decimal customerCharge = 0;

            switch (activeBillingCase)
            {
                case BillingCases.Residential:
                    if (Validator.IsProvided(txtName, "Name") && 
                        Validator.IsProvided(txtGeneralUsage, "Electricity used") &&
                        Validator.IsNoneNegativeDouble(txtGeneralUsage, "Electricity used"))
                    {
                        //Create a new customer 
                        ResidentialCustomer newCustomer = new ResidentialCustomer();

                        
                        //Set customer properties
                        newCustomer.CustomerName = txtName.Text;

                        //Perform calculation for residential billing
                        customerCharge = newCustomer.CalculateCharge(Double.Parse(txtGeneralUsage.Text));

                        //Display results
                        DisplayResults(customerCharge, newCustomer);
                        customers.Add(newCustomer);
                        FillListBox();
                        UpdateTotals();
                    }
                    break;
                case BillingCases.Commercial:
                    if (Validator.IsProvided(txtName, "Name") &&
                        Validator.IsProvided(txtGeneralUsage, "Electricity used") &&
                        Validator.IsNoneNegativeDouble(txtGeneralUsage, "Electricity used"))
                    {
                        //Create a new customer
                        CommercialCustomer newCustomer = new CommercialCustomer();

                        //Set customer properties
                        newCustomer.CustomerName = txtName.Text;

                        //Perform calculation for the commercial billing
                        customerCharge = newCustomer.CalculateCharge(Double.Parse(txtGeneralUsage.Text));

                        //Display results
                        DisplayResults(customerCharge, newCustomer);
                        customers.Add(newCustomer);
                        FillListBox();
                        UpdateTotals();
                    }
                    break;
                case BillingCases.Industrial:
                    if (Validator.IsProvided(txtName, "Name") &&
                        Validator.IsProvided(txtPeakUsage, "Peak electricity usage") &&
                        Validator.IsNoneNegativeDouble(txtPeakUsage, "Peak electricity usage") &&
                        Validator.IsProvided(txtOffPeakUsage, "Off peak electricity usage") &&
                        Validator.IsNoneNegativeDouble(txtOffPeakUsage, "Off peak electricity usage"))
                    {

                        //create new customer
                        IndustrialCustomer newCustomer = new IndustrialCustomer();

                        //set customer properties
                        newCustomer.CustomerName = txtName.Text;

                        //Perform calculation for industrial billing
                        customerCharge = newCustomer.CalculateCharge(Double.Parse(txtPeakUsage.Text), Double.Parse(txtOffPeakUsage.Text));
                        
                        //Display results
                        DisplayResults(customerCharge, newCustomer);
                        customers.Add(newCustomer);
                        FillListBox();
                        UpdateTotals();
                    }
                    break;
            }
            
        }

        /*
         * Method displays the calculated results on UI controls
         * It takes the results as parameters
         * */
        private void DisplayResults(decimal result, Customer myCustomer)
        {
            txtTotalAmt.Text = result.ToString("c");
            txtAccountNo.Text = myCustomer.AccountNumber.ToString();
        }

        //Updates all the text boxes that displays total values 
        private void UpdateTotals()
        {
            txtCustomerCount.Text = customers.Count().ToString();
            txtResidentialTotal.Text = customers.ResidentialTotal.ToString("c");
            txtCommercialTotal.Text = customers.CommercialTotal.ToString("c");
            txtIndustrialTotal.Text = customers.IndustrialTotal.ToString("c");
            txtGrandTotal.Text = customers.GrandTotal.ToString("c");
        }

        /*
         * Method stops the application
         * */
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /*
         * Method clears all inputs and outputs and resets the form to default value
         * for residential customers
         * */
        private void btnClear_Click(object sender, EventArgs e)
        {
            SetFormDefaults();
            clearInputsAndOutputs();
            FocusOnActiveCase();
        }

        /*
         * Method resets the form to the default state for residential customers
         * */
        private void SetFormDefaults()
        {
            radBtnResidential.Checked = true; //Set radio button for residential to checked
            HideUIForPeakUsage(); //Hide the UI for peak usage
            ShowUIForGeneralUsage(); //Show the UI for residential and commercial customers in case it is hiden
            activeBillingCase = BillingCases.Residential;
        }

        /*
         * Method clears all input controls of values
         * */
         private void clearInputsAndOutputs()
        {
            txtGeneralUsage.Text = "";
            txtPeakUsage.Text = "";
            txtOffPeakUsage.Text = "";
            txtTotalAmt.Text = "";
            txtAccountNo.Text = "";
            txtName.Text = "";
        }

        /*
         * Method focus input controls on the textbox of the active case
         * */
        private void FocusOnActiveCase()
        {
            switch (activeBillingCase)
            {
                case BillingCases.Residential:
                    txtGeneralUsage.Focus();
                    break;

                case BillingCases.Commercial:
                    txtGeneralUsage.Focus();
                    break;

                case BillingCases.Industrial:
                    txtPeakUsage.Focus();
                    break;
            }
        }

        //Method is uses delegation to handle changes to the customer list
        private void HandleChange(CustomerList customers)
        {
            customers.Save();
            FillListBox();
        }

        //Method updates the customer list box
        private void FillListBox()
        {
            Customer c;
            lstBoxCustomers.Items.Clear();

            for(int i = 0; i < customers.Count(); i++)
            {
                c = customers[i];
                lstBoxCustomers.Items.Add(c.ToString());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerBillingSystem
{
    /**
    * Author: Prince Nimoh
    * Student #: 000792122
    * Date: July 8, 2018
    * Context: RAD programming lab 2
    * Industrial customer class.
    * It inherits from the customer class.
     * */
    class IndustrialCustomer : Customer
    {
        private const decimal INDUSTRIAL_FLAT_RATE_PEAK = 76m;    //Flat rate for peak usage for industrial customers
        private const decimal INDUSTRIAL_ADDITIONAL_RATE_PEAK = 0.065m; //Additional rate for peak usage above 1000kWh for Industrial customers
        private const decimal INDUSTRIAL_FLAT_RATE_OFFPEAK = 40m; //Flat rate for off peak usage for industrial customers
        private const decimal INDUSTRIAL_ADDITIONAL_RATE_OFFPEAK = 0.028m; //Additional rate for off peak usage above 1000kWh for Industrial customers
        private const double BASE_USAGE_FOR_INDUSTRIAL = 1000;    //Base usage included in industrial flat rate

        /**
         * Default constructor for the industrial customer class
         * */
        public IndustrialCustomer() : base()
        {
            SetupObject();
        }

        /**
         * Constructor for the industrial customer
         * Takes the account number, customer name
         * customer type and customer charge as parameters
         * */
        public IndustrialCustomer(int accountNumber, string name,
                                  decimal charge)
               : base(accountNumber, name, charge)
        {
            SetupObject();
        }

        //Method sets up the values of customertype and accountnumber
        //This method is only intended to be called by the constructor
        private void SetupObject()
        {
            CustomerType = "I";
        }

        /*Method calculates the amount due for industrial customers based on thier usage
         * It receives the amount of electricity used in kWh
         * */
        public override decimal CalculateCharge(double usage, double offPeakUsage)
        {
            //Industrial customers are always charge a flat rate for both peak and off peak usage
            CustomerCharge = INDUSTRIAL_FLAT_RATE_PEAK + INDUSTRIAL_FLAT_RATE_OFFPEAK;
           

            //Calculate the additional cost for peak usage
            if (usage > BASE_USAGE_FOR_INDUSTRIAL)
            {
                CustomerCharge += INDUSTRIAL_ADDITIONAL_RATE_PEAK * (decimal)(usage - BASE_USAGE_FOR_INDUSTRIAL);
            }

            //Calculate the additional cost for off peak usage
            if (offPeakUsage > BASE_USAGE_FOR_INDUSTRIAL)
            {
                CustomerCharge += INDUSTRIAL_ADDITIONAL_RATE_OFFPEAK * (decimal)(offPeakUsage - BASE_USAGE_FOR_INDUSTRIAL);
            }

            return CustomerCharge;
        }
    }
}

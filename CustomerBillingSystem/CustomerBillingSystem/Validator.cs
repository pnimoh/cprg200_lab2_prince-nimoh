﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/**
 * Author: Jolanta Warpechowska-Gruca
 * Context: Demo in class on June 13, 2018
 * Extended by Prince Nimoh
 * Student #: 000792122
 * Date: June 13, 2018
 * The Validator class provides utility methods for validating
 * user inputs from a textbox.
 * */
namespace CustomerBillingSystem
{
    public static class Validator
    {

        //Tests if the text box contains anything
        public static bool IsProvided(TextBox inputBox, string name)
        {
            bool result = true; //innocent until proven guilty

            if (inputBox.Text.Trim() == "")
            {
                result = false;
                MessageBox.Show(name + " has to be provided");
                inputBox.Focus();
            }
            return result;
        }

        //Tests if the text box contains an non-integer value
        public static bool IsNonNegativeInteger(TextBox inputBox, string name)
        {
            bool result = true;
            int val; //to capture the value from TryParse
            if (!Int32.TryParse(inputBox.Text, out val))
            {
                result = false;
                MessageBox.Show(name + " must be a whole number");
                inputBox.SelectAll();
                inputBox.Focus();
            }
            else 
            {
                if (val < 0)
                {
                    result = false;
                    MessageBox.Show(name + " must be zero or more");
                    inputBox.SelectAll();
                    inputBox.Focus();
                }
            }
            return result;
        }

        //Tests if the text box contains a positve double value
        public static bool IsPositiveDouble(TextBox inputBox, string name)
        {
            bool result = true;
            double val; //to capture the value from TryParse
            if (!Double.TryParse(inputBox.Text, out val))
            {
                result = false;
                MessageBox.Show(name + " must be a decimal number");
                inputBox.SelectAll();
                inputBox.Focus();
            }
            else// It is a double
            {
                if (val <= 0)
                {
                    result = false;
                    MessageBox.Show(name + " must be greater than zero");
                    inputBox.SelectAll();
                    inputBox.Focus();
                }
            }
            return result;
        }

        //Tests if the text box contains a non-negative double value including zero
        public static bool IsNoneNegativeDouble(TextBox inputBox, string name)
        {
            bool result = true;
            double val; //to capture the value from TryParse
            if (!Double.TryParse(inputBox.Text, out val))
            {
                result = false;
                MessageBox.Show(name + " must be a decimal number");
                inputBox.SelectAll();
                inputBox.Focus();
            }
            else// It is a double
            {
                if (val < 0)
                {
                    result = false;
                    MessageBox.Show(name + " must be zero or a positive decimal number");
                    inputBox.SelectAll();
                    inputBox.Focus();
                }
            }
            return result;
        }
    }
}

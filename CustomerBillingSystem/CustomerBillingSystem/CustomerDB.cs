﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomerBillingSystem
{
    /**
    * CustomerDB class that writes customers to file
    * Author: Prince Nimoh
    * Context: RAD programming lab 2
    * Reference: ProductList class from textbook example for ProductMaintainance Project
    * Student #: 000792122
    * Date: July 6, 2018
    * */
    class CustomerDB
    {

        private const string path = @"Customers.txt";
        private const int INDEX_OF_ACCOUNT = 0; //Index of the account# when a line from file is split into an array
        private const int INDEX_OF_NAME = 1; //Index of name when a line from file is split into an array
        private const int INDEX_OF_TYPE = 2; //Index of the customer type when a line from file is split into an array
        private const int INDEX_OF_CHARGE = 3; //Index of customer charge when a line from file is split into an array



        public static List<Customer> GetCustomers()
        {
            // create the list
            List<Customer> customersFromFile = new List<Customer>();

            FileStream fs = null;
            StreamReader sr = null;
            string line;
            string[] parts;

            try
            {
                //open the file for reading (the very first time, the file does not exist)
                fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read);
                sr = new StreamReader(fs);

                //read data
                while (!sr.EndOfStream)// while there is still data
                {
                    line = sr.ReadLine();
                    parts = line.Split(','); //split where commas are

                    int accountNo = Int32.Parse(parts[INDEX_OF_ACCOUNT].Trim());
                    string name = parts[INDEX_OF_NAME];
                    string customerType = parts[INDEX_OF_TYPE];
                    decimal customercharge = Decimal.Parse(parts[INDEX_OF_CHARGE]);

                    switch (customerType)
                    {
                        case "R":
                            customersFromFile.Add(new ResidentialCustomer(accountNo, name, customercharge));
                            break;
                        case "C":
                            customersFromFile.Add(new CommercialCustomer(accountNo, name, customercharge));
                            break;
                        case "I":
                            customersFromFile.Add(new IndustrialCustomer(accountNo, name, customercharge));
                            break;
                    }
                }
            }
            //To do add other types of exceptions here
            catch (Exception ex)
            {
                MessageBox.Show("Error while reading data: " + ex.Message, ex.GetType().ToString());
            }
            finally
            {
                if (sr != null) sr.Close();

            }

            return customersFromFile;
        }

        //Method save the customers from a CustomerList to file
        public static void SaveCustomers(List<Customer> customers)
        {
            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
                //open the file for writing ; overwrite old content
                fs = new FileStream(path, FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs);

                //save data

                for (int i = 0; i < customers.Count; i++)
                {
                    sw.WriteLine(customers[i].ToFileString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while writing data: " + ex.Message, ex.GetType().ToString());
            }
            finally
            {
                if (sw != null) sw.Close();
            }
        }
    }
}

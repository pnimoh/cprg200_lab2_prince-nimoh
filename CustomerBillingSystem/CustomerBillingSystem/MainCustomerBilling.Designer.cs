﻿namespace CustomerBillingSystem
{
    partial class MainCustomerBilling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picEnergyTower = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radBtnIndustrial = new System.Windows.Forms.RadioButton();
            this.radBtnCommercial = new System.Windows.Forms.RadioButton();
            this.radBtnResidential = new System.Windows.Forms.RadioButton();
            this.lblGeneralUsage = new System.Windows.Forms.Label();
            this.txtGeneralUsage = new System.Windows.Forms.TextBox();
            this.lblPeakUsage = new System.Windows.Forms.Label();
            this.txtPeakUsage = new System.Windows.Forms.TextBox();
            this.lblOffPeakUsage = new System.Windows.Forms.Label();
            this.txtOffPeakUsage = new System.Windows.Forms.TextBox();
            this.btnCalculateBill = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotalAmt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAccountNo = new System.Windows.Forms.TextBox();
            this.lstBoxCustomers = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtIndustrialTotal = new System.Windows.Forms.TextBox();
            this.txtCommercialTotal = new System.Windows.Forms.TextBox();
            this.txtResidentialTotal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCustomerCount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picEnergyTower)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // picEnergyTower
            // 
            this.picEnergyTower.ImageLocation = "../../images/energy-tower.jpg";
            this.picEnergyTower.Location = new System.Drawing.Point(11, 17);
            this.picEnergyTower.Margin = new System.Windows.Forms.Padding(4);
            this.picEnergyTower.Name = "picEnergyTower";
            this.picEnergyTower.Size = new System.Drawing.Size(326, 475);
            this.picEnergyTower.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEnergyTower.TabIndex = 0;
            this.picEnergyTower.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radBtnIndustrial);
            this.groupBox1.Controls.Add(this.radBtnCommercial);
            this.groupBox1.Controls.Add(this.radBtnResidential);
            this.groupBox1.Location = new System.Drawing.Point(345, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(407, 105);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Customer Type";
            // 
            // radBtnIndustrial
            // 
            this.radBtnIndustrial.AutoSize = true;
            this.radBtnIndustrial.Location = new System.Drawing.Point(267, 49);
            this.radBtnIndustrial.Margin = new System.Windows.Forms.Padding(4);
            this.radBtnIndustrial.Name = "radBtnIndustrial";
            this.radBtnIndustrial.Size = new System.Drawing.Size(79, 20);
            this.radBtnIndustrial.TabIndex = 2;
            this.radBtnIndustrial.TabStop = true;
            this.radBtnIndustrial.Text = "Industrial";
            this.radBtnIndustrial.UseVisualStyleBackColor = true;
            this.radBtnIndustrial.CheckedChanged += new System.EventHandler(this.radBtnIndustrial_CheckedChanged);
            // 
            // radBtnCommercial
            // 
            this.radBtnCommercial.AutoSize = true;
            this.radBtnCommercial.Location = new System.Drawing.Point(150, 49);
            this.radBtnCommercial.Margin = new System.Windows.Forms.Padding(4);
            this.radBtnCommercial.Name = "radBtnCommercial";
            this.radBtnCommercial.Size = new System.Drawing.Size(98, 20);
            this.radBtnCommercial.TabIndex = 1;
            this.radBtnCommercial.TabStop = true;
            this.radBtnCommercial.Text = "Commercial";
            this.radBtnCommercial.UseVisualStyleBackColor = true;
            this.radBtnCommercial.CheckedChanged += new System.EventHandler(this.radBtnCommercial_CheckedChanged);
            // 
            // radBtnResidential
            // 
            this.radBtnResidential.AutoSize = true;
            this.radBtnResidential.Location = new System.Drawing.Point(27, 49);
            this.radBtnResidential.Margin = new System.Windows.Forms.Padding(4);
            this.radBtnResidential.Name = "radBtnResidential";
            this.radBtnResidential.Size = new System.Drawing.Size(94, 20);
            this.radBtnResidential.TabIndex = 0;
            this.radBtnResidential.TabStop = true;
            this.radBtnResidential.Text = "Residential";
            this.radBtnResidential.UseVisualStyleBackColor = true;
            this.radBtnResidential.CheckedChanged += new System.EventHandler(this.radBtnResidential_CheckedChanged);
            // 
            // lblGeneralUsage
            // 
            this.lblGeneralUsage.AutoSize = true;
            this.lblGeneralUsage.Location = new System.Drawing.Point(342, 137);
            this.lblGeneralUsage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGeneralUsage.Name = "lblGeneralUsage";
            this.lblGeneralUsage.Size = new System.Drawing.Size(177, 16);
            this.lblGeneralUsage.TabIndex = 9;
            this.lblGeneralUsage.Text = "Enter electricity used in kWh:";
            // 
            // txtGeneralUsage
            // 
            this.txtGeneralUsage.Location = new System.Drawing.Point(530, 134);
            this.txtGeneralUsage.Margin = new System.Windows.Forms.Padding(4);
            this.txtGeneralUsage.Name = "txtGeneralUsage";
            this.txtGeneralUsage.Size = new System.Drawing.Size(132, 22);
            this.txtGeneralUsage.TabIndex = 2;
            // 
            // lblPeakUsage
            // 
            this.lblPeakUsage.AutoSize = true;
            this.lblPeakUsage.Location = new System.Drawing.Point(361, 169);
            this.lblPeakUsage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPeakUsage.Name = "lblPeakUsage";
            this.lblPeakUsage.Size = new System.Drawing.Size(160, 16);
            this.lblPeakUsage.TabIndex = 10;
            this.lblPeakUsage.Text = "Enter peak usage in kWh:";
            // 
            // txtPeakUsage
            // 
            this.txtPeakUsage.Location = new System.Drawing.Point(530, 166);
            this.txtPeakUsage.Margin = new System.Windows.Forms.Padding(4);
            this.txtPeakUsage.Name = "txtPeakUsage";
            this.txtPeakUsage.Size = new System.Drawing.Size(132, 22);
            this.txtPeakUsage.TabIndex = 3;
            // 
            // lblOffPeakUsage
            // 
            this.lblOffPeakUsage.AutoSize = true;
            this.lblOffPeakUsage.Location = new System.Drawing.Point(342, 203);
            this.lblOffPeakUsage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOffPeakUsage.Name = "lblOffPeakUsage";
            this.lblOffPeakUsage.Size = new System.Drawing.Size(177, 16);
            this.lblOffPeakUsage.TabIndex = 11;
            this.lblOffPeakUsage.Text = "Enter off peak usage in kWh:";
            // 
            // txtOffPeakUsage
            // 
            this.txtOffPeakUsage.Location = new System.Drawing.Point(530, 200);
            this.txtOffPeakUsage.Margin = new System.Windows.Forms.Padding(4);
            this.txtOffPeakUsage.Name = "txtOffPeakUsage";
            this.txtOffPeakUsage.Size = new System.Drawing.Size(132, 22);
            this.txtOffPeakUsage.TabIndex = 4;
            // 
            // btnCalculateBill
            // 
            this.btnCalculateBill.Location = new System.Drawing.Point(382, 504);
            this.btnCalculateBill.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalculateBill.Name = "btnCalculateBill";
            this.btnCalculateBill.Size = new System.Drawing.Size(133, 28);
            this.btnCalculateBill.TabIndex = 6;
            this.btnCalculateBill.Text = "&Calculate";
            this.btnCalculateBill.UseVisualStyleBackColor = true;
            this.btnCalculateBill.Click += new System.EventHandler(this.btnCalculateBill_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(342, 430);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 16);
            this.label3.TabIndex = 15;
            this.label3.Text = "Billing Details are as follows:";
            // 
            // txtTotalAmt
            // 
            this.txtTotalAmt.Enabled = false;
            this.txtTotalAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmt.Location = new System.Drawing.Point(530, 457);
            this.txtTotalAmt.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalAmt.Name = "txtTotalAmt";
            this.txtTotalAmt.Size = new System.Drawing.Size(132, 22);
            this.txtTotalAmt.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(386, 460);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Total Amount Due:";
            // 
            // btnClear
            // 
            this.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClear.Location = new System.Drawing.Point(531, 506);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(131, 25);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(681, 506);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(131, 25);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(379, 276);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "Enter account number:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(377, 246);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Enter Customer Name:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(530, 243);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(222, 22);
            this.txtName.TabIndex = 5;
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Enabled = false;
            this.txtAccountNo.Location = new System.Drawing.Point(530, 273);
            this.txtAccountNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.Size = new System.Drawing.Size(132, 22);
            this.txtAccountNo.TabIndex = 14;
            // 
            // lstBoxCustomers
            // 
            this.lstBoxCustomers.FormattingEnabled = true;
            this.lstBoxCustomers.ItemHeight = 16;
            this.lstBoxCustomers.Location = new System.Drawing.Point(6, 21);
            this.lstBoxCustomers.Name = "lstBoxCustomers";
            this.lstBoxCustomers.Size = new System.Drawing.Size(284, 292);
            this.lstBoxCustomers.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtGrandTotal);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtIndustrialTotal);
            this.groupBox2.Controls.Add(this.txtCommercialTotal);
            this.groupBox2.Controls.Add(this.txtResidentialTotal);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtCustomerCount);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.lstBoxCustomers);
            this.groupBox2.Location = new System.Drawing.Point(759, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(297, 481);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Summary for all customers";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Enabled = false;
            this.txtGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrandTotal.Location = new System.Drawing.Point(158, 440);
            this.txtGrandTotal.Margin = new System.Windows.Forms.Padding(4);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.Size = new System.Drawing.Size(132, 22);
            this.txtGrandTotal.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(55, 443);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 16);
            this.label11.TabIndex = 9;
            this.label11.Text = "Grand Total:";
            // 
            // txtIndustrialTotal
            // 
            this.txtIndustrialTotal.Enabled = false;
            this.txtIndustrialTotal.Location = new System.Drawing.Point(158, 410);
            this.txtIndustrialTotal.Margin = new System.Windows.Forms.Padding(4);
            this.txtIndustrialTotal.Name = "txtIndustrialTotal";
            this.txtIndustrialTotal.Size = new System.Drawing.Size(132, 22);
            this.txtIndustrialTotal.TabIndex = 8;
            // 
            // txtCommercialTotal
            // 
            this.txtCommercialTotal.Enabled = false;
            this.txtCommercialTotal.Location = new System.Drawing.Point(158, 380);
            this.txtCommercialTotal.Margin = new System.Windows.Forms.Padding(4);
            this.txtCommercialTotal.Name = "txtCommercialTotal";
            this.txtCommercialTotal.Size = new System.Drawing.Size(132, 22);
            this.txtCommercialTotal.TabIndex = 6;
            // 
            // txtResidentialTotal
            // 
            this.txtResidentialTotal.Enabled = false;
            this.txtResidentialTotal.Location = new System.Drawing.Point(157, 350);
            this.txtResidentialTotal.Margin = new System.Windows.Forms.Padding(4);
            this.txtResidentialTotal.Name = "txtResidentialTotal";
            this.txtResidentialTotal.Size = new System.Drawing.Size(132, 22);
            this.txtResidentialTotal.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 353);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 16);
            this.label10.TabIndex = 3;
            this.label10.Text = "Total - Residential:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 383);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 16);
            this.label9.TabIndex = 5;
            this.label9.Text = "Total - Commercial:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 413);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Total - Industrial:";
            // 
            // txtCustomerCount
            // 
            this.txtCustomerCount.Enabled = false;
            this.txtCustomerCount.Location = new System.Drawing.Point(158, 320);
            this.txtCustomerCount.Margin = new System.Windows.Forms.Padding(4);
            this.txtCustomerCount.Name = "txtCustomerCount";
            this.txtCustomerCount.Size = new System.Drawing.Size(132, 22);
            this.txtCustomerCount.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 323);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "Total no. of customers:";
            // 
            // MainCustomerBilling
            // 
            this.AcceptButton = this.btnCalculateBill;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.CancelButton = this.btnClear;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtAccountNo);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTotalAmt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCalculateBill);
            this.Controls.Add(this.txtOffPeakUsage);
            this.Controls.Add(this.lblOffPeakUsage);
            this.Controls.Add(this.txtPeakUsage);
            this.Controls.Add(this.lblPeakUsage);
            this.Controls.Add(this.txtGeneralUsage);
            this.Controls.Add(this.lblGeneralUsage);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.picEnergyTower);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainCustomerBilling";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Billing System";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picEnergyTower)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picEnergyTower;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radBtnIndustrial;
        private System.Windows.Forms.RadioButton radBtnCommercial;
        private System.Windows.Forms.RadioButton radBtnResidential;
        private System.Windows.Forms.Label lblGeneralUsage;
        private System.Windows.Forms.TextBox txtGeneralUsage;
        private System.Windows.Forms.Label lblPeakUsage;
        private System.Windows.Forms.TextBox txtPeakUsage;
        private System.Windows.Forms.Label lblOffPeakUsage;
        private System.Windows.Forms.TextBox txtOffPeakUsage;
        private System.Windows.Forms.Button btnCalculateBill;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTotalAmt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtAccountNo;
        private System.Windows.Forms.ListBox lstBoxCustomers;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCustomerCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIndustrialTotal;
        private System.Windows.Forms.TextBox txtCommercialTotal;
        private System.Windows.Forms.TextBox txtResidentialTotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.Label label11;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerBillingSystem
{
    /**
     * Customer class for organizing customer data
     * This is an abstract class; therefore, no instance can be created
     * Author: Prince Nimoh
     * Context: RAD programming lab 2
     * Student #: 000792122
     * Date: June 25, 2018
     * */
    public abstract class Customer
    {
        //static variable that contains the next accountnumber
        static private int nextAccountNumber;
        private const int ONE = 1; //a constant for the number one
       
        //Properties for customer class
        //This is a read only property
        public int AccountNumber { get; }  //Customer account number
        public string CustomerName { get; set; } //Customer's name

        public string CustomerType { get; protected set; } //Customer type is a single character string R, I, or C

        //Charge calculated for the customer.
        public decimal CustomerCharge { get; set; } 

        /**
         * Default constructor for the customer class
         * */
        public Customer()
        {
            AccountNumber = GetNextAccountNumber();
        }

        /**Constructor for the customer class. 
         * it takes the accountnumber, name, customer type
         * and the customer charge as parameters
         * */
        public Customer(int accountNumber, string name, decimal charge)
        {
            AccountNumber = accountNumber;
            CustomerName = name;
            CustomerCharge = charge;
            GetNextAccountNumber();
        }

        //A static property for that controls getting a unique account number
        static private int GetNextAccountNumber()
        {
            //If the account number is zero we want to instantiate it to one
            if (nextAccountNumber == 0)
            {
                nextAccountNumber = ONE;
            }
            return nextAccountNumber++;
        }

        /**
         *Method for the customers charge based on their usage
         * All classes that inherit from this class have to override and implement this method
         * */
        public abstract decimal CalculateCharge(double usage, double offPeakUsage);

        /**
         * To string method for the customer class
         * */
        public override string ToString()
        {
            return AccountNumber.ToString() +" (" + CustomerType + ") " + CustomerName + ": " 
                    + CustomerCharge.ToString("c");
        }
        /**
         * Method returns a string with the customer properties 
         * separated by a commas
         * */
        public string ToFileString()
        {
            return AccountNumber.ToString() + "," + CustomerName + "," 
                   + CustomerType + "," + CustomerCharge.ToString();
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerBillingSystem
{
    /**
    * Author: Prince Nimoh
    * Student #: 000792122
    * Context: RAD programming lab 2
    * Date: July 8, 2018
    * Commerical customer class.
    * It inherits from the customer class.
    * */
    class CommercialCustomer : Customer
    {
        private const decimal COMMERCIAL_FLAT_RATE = 60m; //Flat rate for the first 1000kWh
        private const decimal COMMERCIAL_ADDITIONAL_RATE = 0.045m; //Additional rate for usage above 1000kWh for commercial customers
        private const double BASE_USAGE_FOR_COMMERCIAL = 1000;    //Base usage included in commercial flat rate

        /**
         * Default constructor for the commercial customer class
         * */
        public CommercialCustomer() : base()
        {
            SetupObject();
        }

        /**
         * Constructor for the commercial customer
         * Takes the account number, customer name
         * customer type and customer charge as parameters
         * */
        public CommercialCustomer(int accountNumber, string name,
                                   decimal charge)
               : base(accountNumber, name, charge)
        {
            SetupObject();
        }

        //Method sets up the values of customertype and accountnumber
        //This method is only intended to be called by the constructor
        private void SetupObject()
        {
            CustomerType = "C";
        }


        /*Method calculates the amount due for commercial customers based on thier usage
         * It receives the amount of electricity used in kWh
         * */
        public override decimal CalculateCharge(double usage, double offPeakUsage = 0)
        {
            CustomerCharge = COMMERCIAL_FLAT_RATE; //Commercial customers are always charged a flat rate even when usage is zero

            if (usage > BASE_USAGE_FOR_COMMERCIAL)
            {
                CustomerCharge += COMMERCIAL_ADDITIONAL_RATE * (decimal)(usage - BASE_USAGE_FOR_COMMERCIAL);
            }

            return CustomerCharge;
        }
    }
}

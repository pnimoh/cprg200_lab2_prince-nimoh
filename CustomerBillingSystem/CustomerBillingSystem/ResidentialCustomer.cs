﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerBillingSystem
{
    /**
    * ResidentialCustomer class that inherits from the Customer base class
    * Implements the calculate charge method
    * Author: Prince Nimoh
    * Context: RAD programming lab 2
    * Student #: 000792122
    * Date: June 27, 2018
    * */
    class ResidentialCustomer : Customer
    {
        private const decimal RESIDENTIAL_BASE_RATE = 6.0m; //Base rate charged residential customers even when usage is zero
        private const decimal RESIDENTIAL_ADDITIONAL_RATE = 0.052m; //Rate charged residential customers for usage per kWh

        /**
         * Default constructor for the residential customer class
         * */
        public ResidentialCustomer() : base()
        {
            SetupObject();
        }

        /**
         * Constructor for the residential customer
         * Takes the account number, customer name
         * customer type and customer charge as parameters
         * */
        public ResidentialCustomer(int accountNumber, string name,
                                    decimal charge) 
               : base(accountNumber, name, charge)
        {
            SetupObject();
        }

        //Method sets up the values of customertype and accountnumber
        //This method is only intended to be called by the constructor
        private void SetupObject()
        {
            CustomerType = "R";
        }


        /*Method calculates the amount due for residential customers based on thier usage
         * It receives the amount of electricity used in kWh
         * */
        public override decimal CalculateCharge(double usage, double offPeakUsage = 0)
        {
            
            CustomerCharge = RESIDENTIAL_BASE_RATE;
            CustomerCharge += RESIDENTIAL_ADDITIONAL_RATE * (decimal)usage;

            return CustomerCharge;
        }
    }
}
